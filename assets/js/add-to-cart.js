jQuery(document).ready(function($) {


  var cart_panel = $('.rt-panel-cart');

  // buton ajax archive
  jQuery(document.body).on('added_to_cart', function(elements) {
    cart_panel.retheme_sidepanel({
      action: 'open',
    });
  });


  /*=================================================
  *  AJAX
  =================================================== */

  jQuery('.single_add_to_cart_button').not('.single_add_to_cart_button.disabled').on('click', function(event) {
    event.preventDefault();
    var product_id = jQuery(this).val();
    var variation_id = jQuery('input[name="variation_id"]').val();
    var quantity = jQuery('input[name="quantity"]').val();
    var button = $(this);

    var icon_trigger = $('.header_main .js-cart-total');

    var curent_count = parseInt(icon_trigger.text());




    if (variation_id != '') {
      jQuery.ajax({
        url: rt_product_object.ajaxurl,
        type: 'POST',
        data: 'action=retheme_add_cart_single&product_id=' + product_id + '&variation_id=' + variation_id + '&quantity=' + quantity,
        beforeSend: function(response) {
          button.addClass('loading');
        },
        success: function(results) {
          $('.widget_shopping_cart_content').html(results);
          button.removeClass('loading');

          cart_panel.retheme_sidepanel({
            action: 'open',
          });

          var total_count = parseInt(curent_count)+parseInt(quantity);

          icon_trigger.text(total_count);


        }
      });
    } else {

      jQuery.ajax({
        url: rt_product_object.ajaxurl,
        type: 'POST',
        data: 'action=retheme_add_cart_single&product_id=' + product_id + '&quantity=' + quantity,
        beforeSend: function(response) {
          button.addClass('loading');
        },
        success: function(results) {
          $('.widget_shopping_cart_content').html(results);
          button.removeClass('loading');

          cart_panel.retheme_sidepanel({
            action: 'open',
          });

          var total_count = parseInt(curent_count)+parseInt(variation_id);

          icon_trigger.text(total_count);

        }
      });
    }
  });

  


});

