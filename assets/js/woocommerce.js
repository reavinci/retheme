jQuery(document).ready(function($) {
  /*=================================================;
   /* QTY FORM
   /*================================================= */
  var qty_count = function() {
    var qty = $(".quantity .qty");

    qty.each(function(index) {
      // get id from class
      var id = $(this).attr("id");
      var form_wrapper = $("#" + id).parent();
      var form_id = $("#" + id);

      // insert new DOM with plus and min icon
      form_wrapper.append(
        '<div class="rt-qty js-qty"><span class="rt-qty__min min">-</span><span class="rt-qty__max max">+</span></div>'
      );

      // plus value
      form_wrapper.find(".min").on("click", function() {
        var current = form_id.val();
        if (parseInt(current) + 1 > 2) {
          form_id.val(parseInt(current) - 1);
          $(".js-button-cart-update").prop("disabled", false);
        }
      });

      // min value
      form_wrapper.find(".max").on("click", function() {
        var current = form_id.val();

        form_id.val(parseInt(current) + 1);
        $(".js-button-cart-update").prop("disabled", false);
      });
    });
  };
  qty_count();

  /** Action after cart update */
  $(document.body).on("updated_cart_totals", function() {
    qty_count();
  });

  /*=================================================;
    /* ACCOUNT LOGIN REGISTER
    /*================================================= 
    * @desc toggle form login and register on my account page
    */

  var account_login_register = function() {
    var trigger_login = $(".js-account-login");
    var trigger_register = $(".js-account-register");

    var box_login = $(".rt-form-login");
    var box_register = $(".rt-form-register");

    trigger_login.on("click", function(event) {
      event.preventDefault();

      box_login.show().addClass("is-active");
      box_register.hide().removeClass("is-active");
    });

    trigger_register.on("click", function(event) {
      event.preventDefault();
      box_register.show().addClass("is-active");
      box_login.hide().removeClass("is-active");
    });
  };

  account_login_register();
});
