<?php
namespace Retheme;

class Generate_Code
{

    public function __construct($value = '')
    {

        // if (is_admin_bar_showing()) {
        //     add_action('wp_head', array($this, 'style_admin_bar'), 99);
        // }

        add_action('wp_head', array($this, 'google_analytic'), 99);
        add_action('wp_head', array($this, 'facebook_pixel'), 99);



    }

    public function css_clean($data)
    {
        $result = "";

        foreach ($data as $item) {
            if ($item != "" && substr($item, -1) != '{') {
                $result .= $item . "}";
            }
        }
        return $result;
    }

    public function css_minify($css)
    {
        $css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css);

        /* remove comment */
        $regex = array(
            "`^([\t\s]+)`ism" => '',
            "`^\/\*(.+?)\*\/`ism" => '',
            "`([\n\A;]+)\/\*(.+?)\*\/`ism" => '$1',
            "`([\n\A;\s]+)//(.+?)[\n\r]`ism" => "$1\n",
            "`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism" => "\n",
        );

        $buffer = preg_replace(array_keys($regex), $regex, $css);

        return $buffer;
    }

  
    /**
     * Admin bar
     */
    public function style_admin_bar()
    {
        $css[] = $this->css([
            'name' => 'header_overlay_sticky',
            'element' => '.rt-header.is-overlay, .rt-header.is-sticky',
            'property' => 'top',
            'value' => '32px !important',
        ]);

        echo '<style id="retheme_admin_bar">' . join(' ', $css) . '</style>';

    }


  
    /**
     * add Google Analytic 
     *
     * @return void
     */
    public function google_analytic(){
        if(!empty(rt_get_field('google_analytic', 'option'))):
        ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/<?php rt_get_field('google_analytic', 'option')?>?id=UA-123881472-1"></script>
            <script id="google-analytic">
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-123881472-1');
            </script>

        <?php
        endif;
    }

    /**
     * add Facebook Pixels
     *
     * @return void
     */
    public function facebook_pixel(){
        if(!empty(rt_get_field('facebook_pixel', 'option'))):
        ?>
           <!-- Facebook Pixel oke -->
            <script id="facebook-pixel">
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php rt_get_field('facebook_pixel', 'option')?>');
            fbq('track', 'PageView');

            <?php if(!empty(rt_get_field('pixel_event'))):?>
            fbq('track', '<?php rt_get_field('pixel_event')?>');
            <?php endif?>

            </script>
            <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=".<?php echo rt_get_field('facebook_pixel', 'theme_options')?>."&ev=PageView&noscript=1"/></noscript>
            <!-- End Facebook Pixel Code -->

        <?php
        endif;
    }


    /**
     * helper write css in php file
     *
     * @param array $args
     * @return void
     */
    public function css($args = array())
    {
        if (!empty($args['value'])) {

            $css = '';
            ob_start();

            $css .= $args['element'];
            $css .= '{';
            $css .= $args['property'] . ':' . $args['value'] . ';';
            $css .= '}';
            $css .= ob_get_clean();

            $css_slide = preg_split("/\s*\}/i", $css);
            $css_clean = $this->css_clean($css_slide);

            $minify = $this->css_minify($css_clean);

            return $minify;

        }

    }

    /* end class */
}

new Generate_Code;
