
<?php
$menu_id = $this->get_id();
$classes = 'rt-menu js-elementor-menu rt-menu--'.$settings['layout'];
?>

<div id="<?php echo 'menu-'.$menu_id?>" class="<?php echo $classes?>"
data-animatein='<?php echo rt_option('header_main_submenu_animation_in', 'transition.fadeIn') ?>'
data-duration='<?php echo rt_option('header_main_submenu_animation_duration', '300') ?>'>

<?php
$args = array(
    'menu_id' => 'menu-main-'.$menu_id,
    'menu' => $settings['menu'],
    'menu_class' => 'rt-menu__main',
);

wp_nav_menu($args);

?>
</div>
