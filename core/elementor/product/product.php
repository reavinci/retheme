<?php

namespace Retheme\Elementor;

use Retheme\Elementor_Base;
use Retheme\Helper;
use Elementor;
use Elementor\Controls_Manager;


if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Products extends Elementor_Base
{
    public function get_name()
    {
        return 'retheme-product';
    }

    public function get_title()
    {
        return __('Products', 'rt_domain');
    }

    public function get_icon()
    {
        return 'ate-icon ate-product';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_option();

        $this->style_general();

        $this->setting_carousel();

    }

    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'rt_domain'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'product',
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', 'rt_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'rt_domain'),
                'type' => Controls_Manager::HEADING,
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'rt_domain'),
                    'featured' => __('Featured Products', 'rt_domain'),
                    'category' => __('Category', 'rt_domain'),
                    'manually' => __('Manually Id', 'rt_domain'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Category', 'rt_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'default' => 'lastest',
                'options' => Helper::get_terms('product_cat'),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Product', 'rt_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts('product'),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'rt_domain'),
                    'total_sales' => __('Most Selling', 'rt_domain'),
                    'wp_post_views_count' => __('Most Viewer', 'rt_domain'),
                    'comment_count' => __('Most Review', 'rt_domain'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'rt_domain'),
                    'DESC' => __('DESC', 'rt_domain'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'rt_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'condition' => [
                    'posts_post_type!' => 'by_id',
                ],
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'rt_domain'),
            ]
        );
        $this->end_controls_section();
    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'setting_product',
            [
                'label' => __('Options', 'rt_domain'),
            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'rt_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'your-plugin'),
                'label_off' => __('Off', 'your-plugin'),
                'return_value' => 'no',
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'rt_domain'),
                    2 => __(2, 'rt_domain'),
                    3 => __(3, 'rt_domain'),
                    4 => __(4, 'rt_domain'),
                    6 => __(6, 'rt_domain'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

          $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );


        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'class_wrapper' => 'products woocommerce',
            'template_part' => 'woocommerce/content-product',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));
    }
}