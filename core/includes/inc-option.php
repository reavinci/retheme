<?php
/*==============================================
 * RT OPTION
==============================================
 *  @desc this function get option from customizer
 * if metabox not null this function return data form metabox
 * name customizer and metabox must same
 */
function rt_option($setting, $default = '')
{
    // check default options
    if (!empty(rt_get_field($setting))) {
        if (rt_get_field($setting) !== 'default') {
            $options = rt_get_field($setting);
        } else {
            $options = get_theme_mod($setting, $default);
        }
    } else {
        $options = get_theme_mod($setting, $default);
    }
    
    return apply_filters($setting, $options);

}




/*=================================================;
/* GET FIELD ACF
/*================================================= */
/** This function replace default get field acf */

function rt_get_field($field, $post_id = false, $format_value = false)
{

    if (!class_exists('acf')) {
        return false;
    }

    $value = get_field($field, $post_id, $format_value);

    /**
     * Some values are saved as empty string or 0 for fields (e.g true_false fields).
     * So we used is_null instead of is_empty to check if post meta is set or not.
     */
    if (is_null($value)) {
        return false;

    }

    return $value;
}
