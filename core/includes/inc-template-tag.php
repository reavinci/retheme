<?php

/*=================================================
 *  LIMIT TITLE
/*================================================= 
 * @since 1.0.0
 * @param  number for limit string $length
 * @return string title limit 
 */
function rt_the_title($limit = '')
{

    if (empty($limit)) {
        $limit = '99';
    }
    $title = get_the_title($post->ID);
    if (strlen($title) > $limit) {
        $title = substr($title, 0, $limit) . '...';
    }

    echo $title;
}

/*=================================================
 *  LIMIT CONTENT
/*================================================= */
/**
 * @since 1.0.0
 * @param [length: number for limit string output]
 * @return [string]
 */

function rt_the_content($length = 18)
{
    global $post;

    // Check for custom excerpt
    if (has_excerpt($post->ID) ) {
        $output = $post->post_excerpt;
    }

    // No custom excerpt
    else {

        // Check for more tag and return content if it exists
        if (strpos($post->post_content, '<!--more-->')) {
            $output = apply_filters('the_content', get_the_content());
        }

        // No more tag defined
        else {
            $output = wp_trim_words(strip_shortcodes($post->post_content), $length);
        }

    }

    echo $output;

}


/*=================================================;
/* PAGINATION
/*================================================= */
function rt_pagination()
{
    global $wp_query;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $total_pages = $wp_query->max_num_pages;
    $big = 999999999; // need an unlikely integer


    $loop = apply_filters('rt_loop_query', array(
        'pagination' => 'number',
        'post_type' => 'post',
        'posts_per_page' => 7,
        'template_part' => 'template-parts/post/content-post',
    ));


    $settings = json_encode(array(
        'post_type' => $loop['post_type'] ,
        'posts_per_page' => $loop['posts_per_page'],
        'template_part' =>  $loop['template_part'],
    ));

  
    if ($loop['pagination'] == 'number' && $paged <= $total_pages) {

        echo '<div class="rt-pagination">' .
        paginate_links(array(
            'prev_text' => __('<span class="ti-arrow-left"></span>', 'rt_domain'),
            'next_text' => __('<span class="ti-arrow-right"></span>', 'rt_domain'),
        ))
        . '</div>';
    }

    if ($loop['pagination']== 'loadmore' && $paged != $total_pages) {

        echo '<script>
            var post_archive =' . $settings . ';
          </script>';

        echo '<div class="rt-pagination rt-pagination--loadmore">
              <div class="rt-pagination__spinner js-post_archive-spinner">
                  <i class="fa fa-spinner fa-spin fa-3x"></i>
              </div>
              <a href="#" data-triger-id="post_archive" class="rt-pagination__button js-loop-load rt-btn">' . __('Load More', 'rt_domain') . '</a>
           </div>';
    }
}
add_action('rt_after_loop', 'rt_pagination', 8);

/*=================================================;
/* META HEAD
/*================================================= */
function rt_head_meta()
{

    //DESCRIPTION
    if (is_single()) {
        $title = single_post_title('', false);
    } else {
        $title = rt_option('blogname', 'Wordpress') . ' - ' . rt_option('blogdescription', 'Just another WordPress sites');
    }

    //AUTHOR
    $author = get_bloginfo('author');

    echo '<title>' . $title . '</title>';
    echo '<meta charset="' . get_bloginfo('charset') . '">';
    echo '<meta name="author" content="' . $author . '">';
    echo '<meta name="viewport" content="width=device-width,initial-scale=1">';
    echo '<link rel="shortcut icon" href="' . rt_option('site_icon') . '">';
    echo '<link rel="pingback" href="' . get_bloginfo('pingback_url') . '">';

}

add_action('wp_head', 'rt_head_meta');

/*=================================================;
/* LOOP
/*================================================= */
function rt_loop_none()
{
    echo apply_filters('rt_loop_none', __('Post not found', 'rt_domain'));
}
add_action('rt_loop_none', 'rt_loop_none');

/*=================================================
 * IMAGE PLACE HOLDER
/*================================================= */
function rt_image_placeholder($classes = "")
{
    $classes = !empty($classes) ? 'class="' . $classes . '"' : '';

    echo '<img ' . $classes . ' src="' . get_template_directory_uri() . '/assets/img/image-placeholder.png">';
}
function rt_image_placeholder_url()
{
    echo get_template_directory_uri() . '/assets/img/image-placeholder.png';
}

/*=================================================
 * IMAGE THUMBNAIL
/*================================================= */
function rt_the_post_thumbnail($size = '')
{
    $size = !empty($size) ? $size : 'featured_medium';

    if (has_post_thumbnail()) {
        the_post_thumbnail($size);
    } else {
        // rt_image_placeholder();
    }
}

/*=================================================
 *  COMMENT
/*================================================= */
function rt_comment()
{
    if (comments_open() || get_comments_number()):
        comments_template();
    endif;
}


/*=================================================
 *  POPULAR POST
/*================================================= */
/**
 * set metabox for store count page view
 *
 * @param [type] $postID
 * @return void
 */
function rt_post_popular($postID)
{
    if (!is_admin()) {
        $count_key = 'wp_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if ($count == '') {
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '1');
        } else {
            ++$count;
            update_post_meta($postID, $count_key, $count);
        }
    }
}
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

/**
 * @desc insert popular post on hook action
 */
function rt_set_popular_post()
{
    if (!is_admin()) {
        rt_post_popular(get_the_ID());
    }

}
//add_action('rt_before_loop', 'rt_set_popular_post');

/**
 * @desc get popular post number from metabox each post or page
 * @param $postID
 * @return number
 */
function rt_get_popular_post($postID)
{
    $count = get_post_meta(get_the_ID(), 'wp_post_views_count', true);
    if ($count) {
        $count = $count;
    } else {
        $count = '0';
    }

    return $count;
}



/*=================================================
 *  CONDITION TAGS
/*================================================= 
* @since 1.0.0
* @desc cek plugin WooCommerce active
* @param page WooCommerce page $page
* @return boolean
*/

if (!function_exists('is_woocommerce_activated')) {
    function rt_is_woocommerce($page = '')
    {
        if (class_exists('WooCommerce')) {
            // Rertuns true on WooCommerce Plugins active
            if (empty($page)) {
                return true;
            }

            // all template WooCommerce but cart and checkout not include
            if ($page == 'pages' && is_woocommerce()) {
                return true;
            }

            // Returns true when on the product archive page (shop).
            if ($page == 'shop' && is_shop()) {
                return true;
            }

            if ($page == 'product' && is_product()) {
                return true;
            }

            // Returns true on the customer’s account pages.
            if ($page == 'account_page' && is_account_page()) {
                return true;
            }

            // Returns true on the cart page.
            if ($page == 'cart' && is_cart()) {
                return true;
            }

            // Returns true on the checkout page.
            if ($page == 'checkout' && is_checkout()) {
                return true;
            }

            // Returns true when viewing a WooCommerce endpoint
            if ($page == 'endpoint_url' && is_wc_endpoint_url()) {
                return true;
            }

          
            if ($page == 'category' && is_product_category()) {
                return true;
            }

     
            if ($page == 'tag' && is_product_tag()) {
                return true;
            }
        } else {
            return false;
        }
    }
}

/*=================================================
/* BREADCRUMBS
/*================================================= */
function rt_breadcrumb()
{
    if (function_exists('breadcrumb_trail')) {
        echo "<div class='rt-breadcrumbs'>";
        breadcrumb_trail();
        echo "</div>";
    }
}

/*=================================================;
/* MENU TERMS
/*================================================= */
/**
 * add parent child term  to menu
 *
 * @param [type] $taxonomy
 * @param string $style
 * @return void
 */
function rt_menu_terms($taxonomy, $style = '')
{
    $style = !empty($style) ? $style : 'rt-menu--horizontal';

    $args = array('parent' => 0);

    // get all direct decendants of the $parent
    $terms = get_terms($taxonomy, $args);

    if (count($terms)):

        echo '<div id="menu-' . $taxonomy . '" class="rt-menu ' . $style . ' js-menu">';
        echo '<ul class="rt-menu__main">';

        foreach ($terms as $term) {

            $children = get_term_children($term->term_id, $taxonomy);

            // get parent term
            echo '<li class="rt-menu__item"><a href="' . get_term_link($term->slug, $taxonomy) . '" class="' . $term->slug . '">' . $term->name . '</a>';

            // get child term
            if (count($children)) {
                echo '<ul class="rt-menu__submenu">';
                foreach ($children as $child) {
                    $child = get_term_by('id', $child, $taxonomy);
                    echo '<li class="rt-menu__item"><a href="' . get_term_link($child, $taxonomy) . '" class="' . $child->slug . '">' . $child->name . '</a></li>';
                }
                echo '</ul>';
            }

            echo '</li>'; // end parent menu

        }

        echo '</ul>';
        echo '</div>';

    endif;

}


/*=================================================
/* MAIN TEMPLATE
/*================================================= 
* @since 1.0.0
* @desc  this function get global template. get template part form template-parts folder
* @param url template part $content
*/
function rt_get_template($content){
    get_header();

    do_action('rt_before_wrapper');

        echo '<section id="page-wrapper" '.rt_set_class_raw('rt_page_wrapper_class', ['page-wrapper']).'>';
    
            echo '<div id="page-container" '.rt_set_class_raw('rt_page_container_class', ['page-container', 'flex']).'>';

                do_action('rt_before_content');

                    if(!is_404()){

                
                        if (have_posts()):

                            do_action('rt_before_loop');

                                if($content !== 'woocommerce'): // load file if not WooCommerce page

                                    while (have_posts()): the_post();

                                        rt_get_template_part($content);

                                    endwhile;

                                else: // load file if WooCommerce  page

                                    while (woocommerce_content()): the_post(); 

                                        woocommerce_content();

                                    endwhile;

                                endif;

                            do_action('rt_after_loop');

                        else:
                            
                            rt_get_template_part('page/content-none');

                        endif;
                    
                    }else{
                        rt_get_template_part('page/content-404');
                    }


                do_action('rt_after_content');

                do_action('rt_sidebar');

            echo '</div>';

        echo '</section>';

    do_action('rt_after_wrapper');

    get_footer();
}