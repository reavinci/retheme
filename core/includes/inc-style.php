<?php
/**
 *  Register Style
 *
 *
 * @package retheme
 */
function rt_style()
{
    /* Vendor */
    wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', '2.3.4');

    /* Font */
    wp_deregister_script('kirki-fontawesome');
    wp_enqueue_style('themify-icon', get_template_directory_uri() . '/assets/themify-icons/css/themify-icons.min.css', '1.0.0');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', '4.7.0');

    /* Retheme Style */
    if(rt_is_woocommerce()){
        wp_enqueue_style('retheme-woocommerce-style', get_template_directory_uri() . '/assets/css/retheme-woo.min.css', '1.0.0');
    }
    wp_enqueue_style('retheme-style', get_template_directory_uri() . '/assets/css/retheme.min.css', '1.2.0');

 

}

add_action('wp_enqueue_scripts', 'rt_style');