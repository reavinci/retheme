<?php
/*=================================================
*  COMMENT FORM
/*================================================= */


function rt_comment_form($fields)
{

    $commenter = wp_get_current_commenter();

    $req      = get_option('require_name_email');
    $aria_req = ($req ? " aria-required='true'" : '');
    $html5    = current_theme_supports('html5', 'comment-form') ? 1 : 0;

    $fields = array(
        'author' => '<div class="comment-input"><div class="rt-form">' . '<label class="rt-form__label" for="author">' . __('Full Name', 'rt_domain') . '</label> ' .
        '<input id="author" class="rt-form__input" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' /></div>',

        'url'    => '<div class="rt-form"><label class="rt-form__label" for="url">' . __('Website (optional)') . '</label> ' .
        '<input class="rt-form__input" id="url" name="url" ' . ($html5 ? 'type="url"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_url']) . '" size="30" /></div>',

        'email'  => '<div class="rt-form"><label class="rt-form__label" for="email">' . __('Email') . '</label> ' .
        '<input id="email" class="rt-form__input" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' /></div></div>',
    );

    return $fields;
}
add_filter('comment_form_default_fields', 'rt_comment_form');



function rt_comment_textarea($args)
{
    $form_class = apply_filters('rt_form_class', '');
    
    $args['comment_field'] = '<div class="rt-form">
            <label class="rt-form__label" for="comment">' . _x('Comment', 'rt_domain') . '</label>
            <textarea class="rt-form__input" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
        </div>';
    $args['class_submit'] = 'rt-btn rt-btn--primary';

    return $args;
}
add_filter('comment_form_defaults', 'rt_comment_textarea');

/*=================================================
*  ADD CLASS LINK COMMENT
/*================================================= */
/**
 * Replace link comment classes
 */
function rt_reply_link_class($class)
{
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link rt-comment__reply", $class);

    return $class;
}
add_filter('comment_reply_link', 'rt_reply_link_class');
