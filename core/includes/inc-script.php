<?php
/* *----------------------------------------------------
 * register scripts
 *---------------------------------------------------- */
function rt_scripts()
{



    /* Vendor */
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '2.3.4', true);
    wp_enqueue_script('velocity', get_template_directory_uri() . '/assets/js/velocity.min.js', false, '1.5.0', true);
    wp_enqueue_script('velocity-ui', get_template_directory_uri() . '/assets/js/velocity.ui.min.js', false, '5.2.0', true);
    wp_enqueue_script('sticky-kit', get_template_directory_uri() . '/assets/js/jquery.sticky-kit.min.js', array('jquery'), '1.1.2', true);
    wp_enqueue_script('masonry', get_template_directory_uri() . '/assets/js/masonry.pkgd.min.js', array('jquery'), '4.2.2', true);

   /* Retheme*/
   if(rt_is_woocommerce('product')){
   
       wp_enqueue_script('jquery.photoswipe-global', get_template_directory_uri() . '/assets/js/jquery.photoswipe-global.js', array('jquery'), '1.0.0', true);
       wp_enqueue_script('single-product-woocommerce', get_template_directory_uri() . '/assets/js/single-product.js', array('jquery'), '1.0.0', true);
   }

   if(class_exists('woocommerce')){
       wp_enqueue_script('retheme-woocommerce', get_template_directory_uri() . '/assets/js/woocommerce.min.js', array('jquery'), '1.0.0', true);
   }

  

    wp_enqueue_script('retheme-script', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), '1.0.0', true);

    /* Comment */
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

}

add_action('wp_enqueue_scripts', 'rt_scripts');