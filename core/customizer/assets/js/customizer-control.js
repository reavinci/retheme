jQuery(document).ready(function ($) {

    var api = wp.customize;
    /*=================================================
    * HEADER BUILDER
    /*================================================= */
    /**
     * this function for focus to element header if element in header builder click event
     * 
     * @param {*} element : class each element in source builder
     * @param {*} control : control id each element
     * @param {*} group : class each element
     */
    var control_focus = function (element, control, group) {
        $('.js-builder').find(element).on('click', function () {

            api.control(control).focus();

            if (group) {
                // close all control
                $('.control-collapse').removeClass('is-active').slideUp();
                $('#customize-control-' + control).addClass('is-active');

                // show this control
                $('.control-collapse.' + group).slideDown();

            }


            // control active move to top panel
            var position = $('#customize-control-' + control).position();

            $('.wp-full-overlay-sidebar-content').scrollTop(position.top);


        });


    }

    control_focus('#header-tab-normal .logo', 'brand_logo_primary', '');
    control_focus('#header-tab-sticky .logo', 'brand_logo_sticky', '');
    control_focus('#header-tab-mobile .logo', 'brand_logo_mobile', '');

    control_focus('#header-tab-normal .main-menu', 'retheme_header_header_main_menu', 'header_menu');
    control_focus('#header-tab-sticky .main-menu', 'retheme_header_header_sticky', 'header_sticky');
    control_focus('#header-tab-normal .second-menu', 'retheme_header_header_main_menu', 'header_menu');
    control_focus('#header-tab-sticky .second-menu', 'retheme_header_header_sticky', 'header_sticky');
    control_focus('#header-tab-normal .thirdty-menu', 'retheme_header_header_main_menu', 'header_menu');
    control_focus('#header-tab-sticky .thirdty-menu', 'retheme_header_header_sticky', 'header_sticky');

    control_focus('.topbar-menu', 'retheme_header_header_topbar', 'header_topbar');

    control_focus('#header-tab-normal .search-form', 'header_search_form', 'header_search_form');
    control_focus('#header-tab-sticky .search-form', 'header_search_form', 'header_search_form');
    control_focus('#header-tab-mobile .search-form', 'header_drawer', 'menu_drawer_search');

    control_focus('.social', 'retheme_header_header_social', 'header_socmed');

    control_focus('.button-1', 'retheme_header_header_button_1', 'header_btn_1');
    control_focus('.button-2', 'retheme_header_header_button_2', 'header_btn_2');
    control_focus('.button-3', 'retheme_header_header_button_3', 'header_btn_3');
    control_focus('.html-1', 'retheme_header_header_html_1', 'header_html_1');
    control_focus('.html-2', 'retheme_header_header_html_2', 'header_html_2');
    control_focus('.html-3', 'retheme_header_header_html_3', 'header_html_3');



});