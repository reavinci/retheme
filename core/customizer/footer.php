<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Footer extends Customizer_Base
{

    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        $this->add_option_widget();

        $this->add_option_bottom();

        $this->add_footer_tags();
        $this->add_footer_button();
        $this->add_footer_form();

    }

    public function set_panel()
    {
        $this->add_panel('footer_panel', array(
            'title' => __('Footer', 'rt_domain'),
        ));
    }

    public function set_section()
    {
        $this->add_section('footer_panel', array(
            'footer_widget' => array(esc_attr__('Widget Area', 'rt_domain')),
            'footer_bottom' => array(esc_attr__('Bottom Footer', 'rt_domain')),
            'footer_categories' => array(esc_attr__('Categories', 'rt_domain')),
            'footer_tag' => array(esc_attr__('Tags', 'rt_domain')),
            'footer_button' => array(esc_attr__('Button', 'rt_domain')),
            'footer_form' => array(esc_attr__('Form', 'rt_domain')),
        ));
    }

  
      public function add_option_widget()
        {

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'footer_widget',
            'label' => __('Enable Widget Footer', 'rt_domain'),
            'section' => 'footer_widget_section',
            'default' => true,
        ));



        $this->add_field(array(
            'type' => 'radio-image',
            'section' => 'footer_widget_section',
            'settings' => 'footer_widget_layout',
            'label' => __('Layout', 'rt_domain'),
            'default' => 'footer-1',
            'choices' => array(
                'footer-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-1.svg',
                'footer-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-2.svg',
                'footer-3' => get_template_directory_uri() . '/core/customizer/assets/img/footer-3.svg',
                'footer-elementor' => get_template_directory_uri() . '/core/customizer/assets/img/footer-builder.svg',
            ),
        ));


 
        /**
         * get list from elementor library
         */
        $library = \Retheme\Helper::get_posts('elementor_library');

        $this->add_field(array(
            'type' => 'select',
            'section' => 'footer_widget_section',
            'settings' => 'footer_widget_layout_elementor',
            'label' => __('Elementor Library', 'rt_domain'),
            'multiple' => 1,
            'choices' => $library,
            'active_callback' => array(
                array(
                    'setting' => 'footer_widget_layout',
                    'operator' => '==',
                    'value' => array('footer-elementor'),
                ),
            ),

        ));

     
        $this->add_field_responsive(array(
            'type' => 'typography',
            'settings' => 'footer_widget_heading_typography',
            'label' => __('Heading Typography', 'rt_domain'),
            'section' => 'footer_widget_section',
            'default' => array(
                'variant' => rt_var('font-weight'),
                'font-size' => '',
                'line-height' => '',
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => '.rt-widget--footer .rt-widget__title',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_color(array(
            'label' => __('Heading Color', 'rt_domain'),
            'settings' => 'footer_widget_heading_color',
            'section' => 'footer_widget_section',
            'element' => '.rt-widget--footer .rt-widget__title',
        ));

        $this->add_field_link(array(
            'settings' => 'footer_option_widget_link',
            'section' => 'footer_widget_section',
            'element' => '.rt-widget--footer a',
            'pseudo' => 'hover',
        ));

        $this->add_field_color(array(
            'settings' => 'footer_option_widget_color',
            'section' => 'footer_widget_section',
            'element' => '.page-footer__widget',
        ));



        $this->add_field_color(array(
            'label' => 'Color Accents',
            'settings' => 'footer_option_widget_color_accent',
            'section' => 'footer_widget_section',
            'element' => '.widget_recent_entries.rt-widget--footer .post-date',
        ));

        $this->add_field_background(array(
            'settings' => 'footer_option_widget_background',
            'section' => 'footer_widget_section',
            'element' => '.page-footer__widget',
        ));

        

    }

    public function add_option_bottom()
    {
        $section = 'footer_bottom_section';

         $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'footer_bottom',
            'label' => __('Enable Widget Button', 'rt_domain'),
            'section' => $section,
            'default' => true,
        ));


        $this->add_field(array(
            'type' => 'sortable',
            'settings' => 'footer_bottom_element',
            'label' => __('Element', 'rt_domain'),
             'section' => $section,
            'default' => array(
                'html-1',
            ),
            'choices' => array(
                'bottom-menu' => __('Bottom Menu', 'rt_domain'),
                'social' => __('Social Media', 'rt_domain'),
                'spacer-1' => __('Spacer 1', 'rt_domain'),
                'spacer-2' => __('Spacer 2', 'rt_domain'),
                'html-1' => __('Copyright', 'rt_domain'),
                'image-1' => __('Image', 'rt_domain'),
            ),

        ));

        $this->add_field_responsive(array(
            'type' => 'radio-buttonset',
            'settings' => 'footer_bottom_position',
            'label' => __('Position', 'rt_domain'),
             'section' => $section,
            'default' => 'space-between',
            'choices' => array(
                'space-between' => __('Justify', 'rt_domain'),
                'left' => __('Left', 'rt_domain'),
                'right' => __('Right', 'rt_domain'),
                'center' => __('Center', 'rt_domain'),
            ),
            'transport' => 'auto',
            'output' => array(
                array(
                    'element' => '.page-footer__bottom .page-container',
                    'property' => 'justify-content',
                ),
            ),
            
        ));

        $this->add_field(array(
            'label' => 'Copyright (Support HTML tag)',
            'settings' => "footer_bottom_text",
            'section' => $section,
            "default" => "@Copyright ".rt_var('product-name').". All Rights Reserved",
            'type' => 'textarea',
        ));

          $this->add_field(array(
            'type' => 'image',
            'settings' => "footer_bottom_img_1",
            'section' => $section,
            'label' => __('Image', 'rt_domain'),
            'description' => __('You can upload image like payment logo, secure logo etc', 'rt_domain'),
        ));



        $this->add_field_color(array(
            'settings' => 'footer_option_bottom_color',
            'section' => $section,
            'element' => '.page-footer__bottom',
        ));

        $this->add_field_link(array(
            'settings' => 'footer_option_bottom_link',
            'section' => $section,
            'element' => '.page-footer__bottom a,
                    .page-footer__bottom .rt-menu--bar-simple li a',
            'pseudo' => 'hover',
        ));

          $this->add_field_background(array(
            'settings' => 'footer_option_bottom_background',
            'section' => $section,
            'element' => '.page-footer__bottom',
        ));

         $this->add_field_border_color(array(
            'settings' => 'footer_option_bottom_border_color',
            'section' => $section,
            'element' => '.page-footer__bottom',
        ));

    }

  
      


    public function add_footer_tags()
    {

        $this->add_field_background(array(
            'settings' => 'footer_tag_background',
            'section' => 'footer_tag_section',
            'element' => '.rt-widget--footer .tag-cloud-link',
            'pseudo' => 'hover',
        ));

        $this->add_field_color(array(
            'settings' => 'footer_tag_color',
            'section' => 'footer_tag_section',
            'element' => '.rt-widget--footer .tag-cloud-link',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_color(array(
            'settings' => 'footer_tag_border_color',
            'section' => 'footer_tag_section',
            'element' => '.rt-widget--footer .tag-cloud-link',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_radius(array(
            'settings' => 'footer_tag_border_radius',
            'section' => 'footer_tag_section',
            'element' => '.rt-widget--footer .tag-cloud-link',
        ));

    }



    public function add_footer_button()
    {

        $this->add_field_button(array(
            'settings' => 'footer_button',
            'section' => 'footer_button_section',
            'element' => '.rt-widget--footer .rt-btn,
										.rt-widget--footer input[type="submit"],
										.rt-widget--footer button',
        ));
    }

    public function add_footer_form()
    {
        $this->add_field_color(array(
            'label' => 'Label Color',
            'settings' => 'footer_form_label',
            'section' => 'footer_form_section',
            'element' => '.rt-widget--footer label',
        ));

        $this->add_field_form(array(
            'settings' => 'footer_form',
            'section' => 'footer_form_section',
            'element' => '.rt-widget--footer .rt-form__input,
										.rt-widget--footer input[type="text"],
										.rt-widget--footer input[type="email"],
										.rt-widget--footer input[type="number"]',
        ));
    }

// end class
}

new Footer;
