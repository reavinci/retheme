<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Layout extends Customizer_Base {

	function __construct() {
		$this->set_section();
		$this->add_setting();
	}

	function set_section() {
		$this->add_section('', array(
			'layout' => array(esc_attr__('Layout', 'rt_domain')),
		));
	}

	function add_setting() {

		$this->add_header(array(
			'label' => 'Options',
			'settings' => 'layout_setting',
			'section' => 'layout_section',
			'class' => 'layout_setting',
		));

		$this->add_field(array(
			'type' => 'radio-image',
			'section' => 'layout_section',
			'class' => 'layout_setting',
			'settings' => 'layout_setting_style',
			'label' => __('Style', 'rt_domain'),
			'desc' => __('Choose the global layout for your website!', 'rt_domain'),
			'default' => 'full',
			'choices' => array(
				'full' => get_template_directory_uri() . '/core/customizer/assets/img/layout-full.svg',
				'boxed' => get_template_directory_uri() . '/core/customizer/assets/img/layout-boxed.svg',
				'fluid' => get_template_directory_uri() . '/core/customizer/assets/img/layout-fluid.svg',
				'frame' => get_template_directory_uri() . '/core/customizer/assets/img/layout-frame.svg',
			),
		));

		$this->add_field(array(
			'type' => 'slider',
			'settings' => 'layout_setting_width',
			'label' => __('Container Width (px)', 'rt_domain'),
			'desc' => __('Apply a width your content layout ', 'rt_domain'),
			'section' => 'layout_section',
			'class' => 'layout_setting',
			'default' => 1170,
			'choices' => array(
				'min' => '720',
				'max' => '1400',
				'step' => '1',
			),
			'active_callback' => array(
				array(
					'setting' => 'layout_setting_style',
					'operator' => '!=',
					'value' => 'fluid',
				),

			),
			'output' => array(
				array(
					'element' => '.page-container,
								.template--boxed .page-main,
								.template--boxed .rt-header.is-sticky,
								.template--boxed .rt-header.is-overlay',
					'property' => 'max-width',
					'units' => 'px',
					'media_query' => $this->breakpoint_large,
					'suffix' => '!important',
				),
			),
			'transport' => 'auto',
		));

		$this->add_field(array(
			'type' => 'background',
			'settings' => 'layout_setting_background',
			'label' => __('Background', 'rt_domain'),
			'section' => 'layout_section',
			'class' => 'layout_setting',
			'default' => array(
				'background-color' => '#fff',
				'background-image' => '',
				'background-repeat' => 'repeat',
				'background-position' => 'center center',
				'background-size' => 'cover',
				'background-attachment' => 'scroll',
			),
			'output' => array(
				array(
					'element' => 'body,
								  body.template--frame,
								  body.template--boxed',
				),
			),
			'transport' => 'auto',
		));

	}

	function add_content() {
		$this->add_header(array(
			'label' => 'Content',
			'settings' => 'layout_content',
			'section' => 'layout_section',
			'class' => 'layout_content',
		));

		$this->add_field_background(array(
			'settings' => 'layout_content_background',
			'section' => 'layout_section',
			'class' => 'layout_content',
			'element' => '.page-wrapper .page-container',
		));

		$this->add_field_border_color(array(
			'settings' => 'layout_content_border_color',
			'section' => 'layout_section',
			'class' => 'layout_content',
			'element' => '.page-wrapper .page-container',
		));

		$this->add_field_responsive(array(
			'type' => 'dimensions',
			'settings' => 'layout_content_padding',
			'label' => __('Content Padding (px)', 'rt_domain'),
			'section' => 'layout_section',
			'class' => 'layout_content',
			'default' => array(
				'top' => '0',
				'right' => '0',
				'bottom' => '0',
				'left' => '0',
			),
			'output' => array(
				array(
					'element' => '.page-wrapper .page-container',
					'property' => 'padding',
					'suffix' => '!important',
				),

			),

			'transport' => 'auto',
		));

	}

// end class
}

New Layout;
