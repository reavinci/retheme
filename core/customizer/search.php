<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Search extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();

        $this->add_search_option();
        // $this->add_search_animation();

    }

    public function set_section()
    {
        $this->add_section('', array(
            'search_option' => array(esc_attr__('Search ', 'rt_domain')),
        ));
    }

    public function add_search_option()
    {
        $section = 'search_option_section';
        $settings = 'search_options';


        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            
            'settings' => $settings . '_style',
            'label' => __('Style', 'rt_domain'),
            'default' => rt_var('search-style'),
            'multiple' => 1,
            'choices' => array(
                'dropdown' => __('Drop Down', 'rt_domain'),
                'overlay' => __('Overlay', 'rt_domain'),
                'modal' => __('Modal', 'rt_domain'),
            ),

        ));

        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            'settings' => $settings . '_result',
            'label' => __('Post Types Result', 'rt_domain'),
            'default' => 'post',
            'multiple' => 1,
            'choices' => apply_filters('rt_post_type_search_result', array(
                'post' => 'Post',
                'product' => 'Product',
            )),
        ));

        $this->add_field(array(
            'type' => 'text',
            'section' => $section,
            
            'settings' => $settings . '_text',
            'label' => __('Placeholder', 'rt_domain'),
            'default' => 'Type Something and enter',
        ));

        $this->add_field_color(array(
            'settings' => $settings . '_color',
            'section' => $section,
            'element' => '.rt-search .rt-search__input,
						 .rt-search .rt-search__icon',
        ));

        $this->add_field_background(array(
            'settings' => $settings . '_background',
            'section' => $section,
            'element' => '.rt-search',
        ));

         $this->add_field_border_color(array(
            'settings' => $settings . '_border',
            'section' => $section,
            'element' => '.rt-search',
        ));

    }

    public function add_search_animation()
    {
        $section = 'search_option_section';

        $this->add_header(array(
            'label' => 'Search - Animation',
            'settings' => 'search_animation',
            'section' => $section,
            'class' => 'search_animation',
        ));
        $this->add_field_animation(array(
            'settings' => 'search_animation',
            'section' => $section,
            'class' => 'search_animation',
        ));
    }

// end class
}

new Search;
