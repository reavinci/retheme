<?php
include_once dirname(__FILE__) . '/vendor/vendor-init.php';
include_once dirname(__FILE__) . '/includes/inc-init.php';
include_once dirname(__FILE__) . '/admin/admin-init.php';
include_once dirname(__FILE__) . '/classes/class-init.php';
include_once dirname(__FILE__) . '/elementor/elementor-init.php';
include_once dirname(__FILE__) . '/customizer/customizer-init.php';
include_once dirname(__FILE__) . '/widget/widget-init.php';
include_once dirname(__FILE__) . '/metabox/metabox-init.php';
include_once dirname(__FILE__) . '/option/option-init.php';

