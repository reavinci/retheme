<?php
if ( ! class_exists( 'Kirki' ) ) {
    require_once dirname(__FILE__) . '/kirki/kirki.php';
}
require_once dirname(__FILE__) . '/breadcrumb-tail.php';
require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';
require_once dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';

$theme_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/retheme/', get_template_directory(), 'retheme');
$theme_update->setBranch('stable_release');