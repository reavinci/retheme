<?php
namespace Retheme\Widget;

class Widget_Company extends \WP_Widget {

	public function __construct() {

		$args = array(
			'classname' => 'retheme-widget-company',
		);

		parent::__construct('retheme_widget_company', 'Retheme - Company', $args);
	}

	// VIEW
	public function widget($args, $instance) {

		// outputs the content of the widget
		if (!isset($args['widget_id'])) {
			$args['widget_id'] = $this->id;
		}

		// widget ID with prefix for use in ACF API functions
		$widget_id = 'widget_' . $args['widget_id'];


		$title = rt_get_field('title', $widget_id);
		$logo = rt_get_field('logo', $widget_id);
		$desc = rt_get_field('description', $widget_id);
		$address = rt_get_field('address', $widget_id);
		$phone = rt_get_field('phone', $widget_id);
		$email = rt_get_field('email', $widget_id);
		$website = rt_get_field('website', $widget_id);


		echo $args['before_widget'];

		if ($title) {
			echo $args['before_title'] . esc_html($title) . $args['after_title'];
		}

		include dirname(__FILE__) . '/widget-company-view.php';

		echo $args['after_widget'];
	}

	// BACKEND
	public function form($instance) {

		
	}

	// UPDATE
	public function update($new_instance, $old_instance) {
		
	}

}
