<div class="rt-company">

  <?php if(!empty($logo)): ?>
  <div class="rt-company__logo rt-img"><?php echo wp_get_attachment_image($logo, 'full') ?></div>
  <?php endif ?>

   <?php if(!empty($desc)): ?>
  <div class="rt-company__desc"><?php  echo $desc ?></div>
  <?php endif ?>

  <ul class="rt-company__list">

    <?php if(!empty($address)): ?>
    <li><i class="fa fa-location-arrow"></i><?php echo $address ?></li>
    <?php endif ?>

    <?php if(!empty($phone)): ?>
    <li><i class="fa fa-phone"></i><?php echo $phone ?></li>
    <?php endif ?>

    <?php if(!empty($email)): ?>
    <li><i class="fa fa-envelope-open"></i><?php echo $email ?></li>
    <?php endif ?>

     <?php if (!empty($website)): ?>
    <li><i class="fa fa-globe-americas"></i><?php echo $website ?></li>
    <?php endif?>


  </ul>
</div>