<div class="rt-widget__body">

  <div <?php rt_set_class('', $classes) ?>>

    <?php if(is_array(rt_option('social_item'))): ?>
      <?php foreach (rt_option('social_item') as $key => $value): ?>

        <a href="<?php echo $value['link_url'] ?>" class="rt-socmed__item <?php echo $value['link_text'] ?>">
              <i class="<?php echo rt_get_fontawesome_class($value['link_text']) ?>"></i>
        </a>

      <?php endforeach; ?>
    <?php endif ?>

  </div>

</div>