<?php
namespace Retheme\Widget;

use Retheme\HTML;
use Retheme\Helper;

class Widget_Posts extends \WP_Widget
{
	public function __construct()
	{
		$args = array(
			'classname' => 'retheme-widget-get-post',
		);

		parent::__construct('retheme_widget_posts', 'Retheme - Posts', $args);
	}

	public function widget($args, $instance)
	{
	
		// outputs the content of the widget
		if (!isset($args['widget_id'])) {
			$args['widget_id'] = $this->id;
		}

		// widget ID with prefix for use in ACF API functions
		$widget_id = 'widget_' . $args['widget_id'];


		$title = rt_get_field('title', $widget_id);

		echo $args['before_widget'];

		if ($title) {
			echo $args['before_title'] . esc_html($title) . $args['after_title'];
		}

		$query_args = array(
			'post_type' => 'post',
			'query_by' => rt_get_field('query_by', $widget_id),
			'orderby' => rt_get_field('order_by', $widget_id),
			'posts_per_page' => rt_get_field('posts_per_page', $widget_id),
			'offset' => rt_get_field('offset', $widget_id),
			'post_id' => rt_get_field('manually', $widget_id),
			'category' => rt_get_field('category', $widget_id),
			'tags' => rt_get_field('tags', $widget_id),
		);



		$the_query = new \WP_Query(Helper::query($query_args));

		if ($the_query->have_posts()) {

			echo HTML::open('rt-post-widget');

			while ($the_query->have_posts()) : $the_query->the_post();

			include dirname(__FILE__) . '/widget-posts-view.php';

			endwhile;
			wp_reset_postdata();

			echo HTML::close();

		} else {
			_e('No Result', 'rt_domain');
		}

		 

		echo $args['after_widget'];
	}

	// BACKEND
	public function form($instance)
	{


	}


	public function update($new_instance, $old_instance)
	{

	}
}
