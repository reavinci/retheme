

<?php do_settings_sections("retheme_section");?>

<div class="bulma">
    <h1 class="title"><?php echo rt_var('product-name') ?></h1>


    <div class="panel bg-light">

        <div class="panel-heading">
            License Information

            <?php if (rt_is_premium()): ?>
            <span class="tag is-success">Activate</span>
            <?php else: ?>
            <span class="tag is-warning">Deactivate</span>
            <?php endif?>

        </div>

        <div class="panel-block">
            <div>
                <div style="margin: 15px 0;">To unlock updates, please enter your license key below. If you don't have a licence key, please see details & pricing.</div>

                <form action="" method="post">
                    <div class="field">
                        <label class="label">License Key</label>
                        <?php if (rt_is_premium()): ?>
                        <input class="input is-disabled" id="theme_license_key" name="theme_license_key" type="password" value="<?php echo get_option(rt_var('product-slug', '_key')); ?>"/>
                        <?php else: ?>
                         <input class="input" id="theme_license_key" name="theme_license_key" type="text" value="<?php echo get_option(rt_var('product-slug', '_key')); ?>"/>
                        <?php endif?>
                    </div>
                    <div class="submit">
                        <?php if (!rt_is_premium()): ?>
                        <input class="button is-info" name="theme_license_submit" type="submit" value="Activate License"/>
                        <?php else: ?>
                        <input class="button is-info" name="theme_license_submit" type="submit" value="Deactivate License"/>
                        <?php endif?>
                    </div>
                </form>

            </div>

        </div>

    </div>

     

</div>