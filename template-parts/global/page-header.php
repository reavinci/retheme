<?php
$title = '';
$desc = '';

$classes[] = 'page-header';
$classes[] = 'page-header--' . rt_option('page_header_style', 'left');

$title = get_the_title();

if (is_archive()) {
    $title = get_the_archive_title();
    $desc = get_the_archive_description();
}

if (is_home()) {
    $title = __('Lastest Post', 'rt_domain');
}

if (is_404()) {
    $title = __('404', 'rt_domain');
}

if (rt_is_woocommerce('shop')) {
    $title = __('Shop', 'rt_domain');
    $desc = '';
}

if (is_search()) {
    $title = __('Search Results for: ', 'rt_domain') . '<span>' . get_search_query() . '</span>';
    $desc = '';
}

$page_title = apply_filters('rt_page_title', $title);
?>


<section id="page-header" <?php rt_set_class('rt_page_header_class', $classes)?>>
  <div class="page-container">

    <?php do_action('rt_page_header_prepend')?>

    <?php if ($page_title): ?>
    <div class="page-header__inner">

      <h1 class="page-header__title"><?php echo $page_title ?></h1>
      
      <?php if (!empty($desc)): ?>
      <div class="page-header__desc">
        <?php echo apply_filters('rt_page_header_desc', $desc) ?>
      </div>
      <?php endif;?>

    </div>
    <?php endif?>


   <?php rt_breadcrumb()?>

    <?php do_action('rt_page_header_append')?>

  </div>
</section>