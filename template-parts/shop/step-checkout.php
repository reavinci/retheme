<?php 
    /**
     * This part only work on WooCommerce
     */
    $shop_class = (rt_is_woocommerce('shop'))? 'is-active':'';
    $cart_class = (rt_is_woocommerce('cart'))? 'is-active':'';
    $checkout_class = (rt_is_woocommerce('checkout'))? 'is-active':'';
?>
<?php if(rt_is_woocommerce('shop') || rt_is_woocommerce('cart') || rt_is_woocommerce('checkout')): ?>
    <ul class="rt-step-checkout">
        <li>
            <a class="<?php echo $shop_class?>" href="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
                <?php _e('Shop', 'rt_domain'); ?> 
            </a>
        </li>
        <li>
            <a class="<?php echo $cart_class?>" href="<?php echo get_permalink(wc_get_page_id('cart')); ?>"> 
                <?php _e('Cart', 'rt_domain') ?> 
            </a>
        </li>
        <li>
            <a class="<?php echo $checkout_class?>" href="<?php echo get_permalink(wc_get_page_id('checkout')); ?>"> 
                <?php _e('Checkout', 'rt_domain') ?>
            </a>
        </li>
    </ul>
<?php endif ?>