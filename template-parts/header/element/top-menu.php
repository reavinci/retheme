<?php
if (has_nav_menu('topbar')) {
	$args = array(

		'title_li' => '',
		'container' => 'top-menu',
		'theme_location' => 'topbar',
		'items_wrap' => '<div id="topbar-menu" class="rt-menu rt-menu--horizontal"><ul class="rt-menu__main">%3$s</ul></div>',

	);
	wp_nav_menu($args);
}
