<?php if (rt_option('header_html_2')) : ?>
  <div id="html-2" class="rt-header__element rt-header__html">

  <?php if (rt_option('header_html_2_shortcode')) : ?>
    <?php echo do_shortcode(rt_option('header_html_2')) ?>
  <?php else : ?>

      <?php if (rt_option('header_html_2_icon')): ?>
          <i class="<?php echo 'mr-5 fa fa-' . rt_option('header_html_2_icon') ?>"></i>
      <?php endif;?>


      <?php echo rt_option('header_html_2', 'Your Text/HTML') ?>
  <?php endif; ?>

  </div>
<?php endif ?>