<?php
$target = '';
if (rt_option('header_button_1_link')) {
  $target = 'target="'.rt_option('header_button_1_link_target', 'blank').'"';
}
?>
<div id="button-1" class="rt-header__element rt-header__btn">
  <a href="<?php echo rt_option('header_button_1_link', '#') ?>" class="rt-btn rt-btn--1 mb-0" <?php echo $target  ?>>

    <?php if (rt_option('header_button_1_icon')): ?>
      <i class="<?php echo 'fa fa-'.rt_option('header_button_1_icon') ?>"></i>
    <?php endif; ?>

    <span><?php echo rt_option('header_button_1_text', 'Button 1') ?></span>
  </a>
</div>
