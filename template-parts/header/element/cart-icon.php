<div id="cart-icon" class="rt-header__element rt-header__cart">
  <i class="rt-header__cart-icon ti-shopping-cart js-cart-trigger"></i>
  <span class="rt-header__cart-count js-cart-total">0</span>
</div>
