<?php
$options = rt_option('header_builder_option');
$defaults = array(
  'main_left_display' => 'normal',
  'main_left_alignment' => 'left',
  'main_center_display' => 'grow',
  'main_center_alignment' => 'left',
  'main_right_display' => 'normal',
  'main_right_alignment' => 'left',
);

$elements = wp_parse_args($options, $defaults);
?>
<?php do_action('rt_before_header_main')?>

<div id="header-primary" class="rt-header__main">
    <div class="page-container">

       <div id="header-main-left" class="rt-header__column" data-alignment="<?php echo $elements['main_left_alignment']?>" data-display="<?php echo $elements['main_left_display']?>">
          <?php do_action('rt_header_main_left')?>
      </div>
      
       <div id="header-main-center" class="rt-header__column" data-alignment="<?php echo $elements['main_center_alignment']?>" data-display="<?php echo $elements['main_center_display']?>">
          <?php do_action('rt_header_main_center')?>
      </div>

       <div id="header-main-right" class="rt-header__column" data-alignment="<?php echo $elements['main_right_alignment']?>" data-display="<?php echo $elements['main_right_display']?>">
           <?php do_action('rt_header_main_right')?>
      </div>
      
    </div>
  </div>
<?php do_action('rt_after_header_main')?>
