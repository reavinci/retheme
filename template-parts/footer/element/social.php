<?php if (rt_option('social_item')): ?>

  <div id="footer-social" class="rt-socmed rt-socmed---sm rt-socmed--simple">
    <?php foreach (rt_option('social_item') as $key => $value): ?>

      <a href="<?php echo $value['link_url'] ?>" class="rt-socmed__item <?php echo $value['link_text'] ?>">
            <i class="<?php echo rt_get_fontawesome_class($value['link_text']) ?>"></i>
      </a>
    <?php endforeach; ?>

  </div>
<?php endif; ?>
