<?php
if (has_nav_menu('footer')) {
	$args = array(

		'title_li' => '',
		'container' => '',
		'theme_location' => 'footer',
		'items_wrap' => '<ul id="footer-bottom-menu" class="rt-menu rt-menu--bar-simple">%3$s</ul>',

	);
	wp_nav_menu($args);
}
