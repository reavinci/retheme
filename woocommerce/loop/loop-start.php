<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if (! defined('ABSPATH')) {
    exit;
}
?>
<?php
global $wp_query;

$classes[] = 'rt-product-archive products flex flex-row flex-loop';


/* Column */
$column = 12 / rt_option('woocommerce_archive_options_column', 3);
$column_md = 12 / rt_option('woocommerce_archive_options_column_tablet', 2);
$column_sm = 12 / rt_option('woocommerce_archive_options_column_mobile', 2);

$classes[] = 'flex-cols-md-' . $column;
$classes[] = 'flex-cols-sm-' . $column_md;
$classes[] = 'flex-cols-xs-' . $column_sm;


if (rt_option('woocommerce_archive_masonry', false)) {
    $classes[] = 'js-masonry';
}
 ?>
<div id="post_archive" <?php rt_set_class('rt_product_class_wrapper', $classes) ?> data-page="1" data-max-paged='<?php echo $wp_query->max_num_pages ?>'>